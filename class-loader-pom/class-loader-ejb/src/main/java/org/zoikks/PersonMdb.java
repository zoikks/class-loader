package org.zoikks;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLClassLoader;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Properties;

import javax.ejb.MessageDriven;
import javax.jms.Message;
import javax.jms.MessageListener;
import javax.jms.TextMessage;

import org.apache.log4j.Logger;

@MessageDriven(name = "PersonMdb", mappedName = "PersonMdb")
public class PersonMdb implements MessageListener {

    private static final String PROPS_FILE_PATH = "mdb-settings/mdb-settings.properties";

    private static Logger logger = Logger.getLogger(PersonMdb.class);

    private PersonUtility personUtility;
    private Date fileDate;
    private Properties props;

    public PersonMdb() {
        props = new Properties();
    }

    @Override
    public void onMessage(Message message) {

        this.readPropertiesFile();

        TextMessage textMessage = (TextMessage) message;

        try {

            String classType = this.props.getProperty(textMessage.getText());

            List<URL> urls = this.loadJars();

            URLClassLoader cl = new URLClassLoader(urls.toArray(new URL[urls.size()]), this.getClass().getClassLoader());

            Class<?> o = cl.loadClass(classType);
            this.personUtility = (PersonUtility) o.newInstance();

            Person person = this.personUtility.getPerson();
            logger.debug("$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$");
            logger.debug("Person Name: " + person.getName());
            logger.debug("$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$");
        } catch (IllegalAccessException e) {
            logger.error(e);
        } catch (ClassNotFoundException e) {
            logger.error(e);
        } catch (InstantiationException e) {
            logger.error(e);
        } catch (Exception e) {
            logger.error(e);
        }
    }

    private List<URL> loadJars() {

        File[] files = new File("./jars").listFiles();

        List<URL> urls = new ArrayList<URL>();

        for (File file : files) {

            if (file.getName().toLowerCase().endsWith(".jar")) {
                try {
                    urls.add(file.toURI().toURL());
                } catch (MalformedURLException e) {
                    logger.error(e);
                }
            }
        }

        return urls;
    }

    private void readPropertiesFile() {

        if (!isPropertiesFileNew()) {
            return;
        }

        try {
            File file = new File(PROPS_FILE_PATH);
            FileInputStream fis = new FileInputStream(file);
            props.load(fis);
        } catch (IOException e) {
            logger.error(e.getMessage());
        } catch (Exception e) {
            logger.error(e.getMessage());
        }
    }

    private boolean isPropertiesFileNew() {

        File file = new File(PROPS_FILE_PATH);

        long lastModifiedDate = file.lastModified();

        if (this.fileDate == null || lastModifiedDate > fileDate.getTime()) {
            this.fileDate = new Date(lastModifiedDate);
            return true;
        }

        return false;
    }
}