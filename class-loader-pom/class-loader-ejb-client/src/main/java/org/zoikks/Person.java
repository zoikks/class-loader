package org.zoikks;

import org.apache.commons.lang3.StringUtils;


public class Person {

    private String name;
    
    public Person(String name) {
        
        if (StringUtils.isBlank(name)) {
            throw new IllegalArgumentException("Person.ctor() - Arguments cannot be null or blank.");
        }
        
        this.name = name;
    }
    
    public String getName() {
        return this.name;
    }
}